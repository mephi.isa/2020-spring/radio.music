import unittest
from datetime import time

import classes


class TestGenres(unittest.TestCase):
    def setUp(self):
        self.gen = classes.Genres(1, "Rock")

    def test_get_id(self):
        self.assertEqual(1, self.gen.get_genre_id())

    def test_set_get_genre(self):
        self.gen.set_genre("Metal")
        self.assertEqual("Metal", self.gen.get_genre())


class TestAlbum(unittest.TestCase):
    def setUp(self):
        self.alb = classes.Album(1, "The Best of Led Zeppelin")

    def test_get_id(self):
        self.assertEqual(1, self.alb.get_album_id())

    def test_set_get_title(self):
        self.assertEqual("The Best of Led Zeppelin", self.alb.get_title())
        self.alb.set_title("Led Zeppelin III")
        self.assertEqual("Led Zeppelin III", self.alb.get_title())

    def test_add_del_composition(self):
        com1 = classes.MusicComposition(2, "Escape", [(1, "Metallica")], [(1, "Ride the Lightning")], 1984, time(0, 3, 41), None, 1, [None])
        com2 = classes.MusicComposition(3, "Nightquest", [(2, "Nightwish")], [(2, "Oceanborn")], 1998, time(0, 4, 15), None, 2, [None])
        self.assertFalse(self.alb.get_all_composition())
        self.alb.add_composition(com1)#добавили 1 композицию
        self.alb.add_composition(com2)#добавили 2 композицию
        #self.assertEqual([com1, com2], self.alb.get_all_composition(self.alb.album_id))#можно так
        self.assertListEqual([com1, com2], self.alb.get_all_composition())#но так более правильно кмк
        self.alb.delete_composition(com1)#удалили 1 композицию
        self.assertListEqual([com2], self.alb.get_all_composition())#проверили что есть только вторая
        self.alb.delete_composition(com2)#удалили 2 композицию
        self.assertFalse(self.alb.get_all_composition())#проверили что ничего не осталось


class TestArtist(unittest.TestCase):
    def setUp(self):
        self.art = classes.Artist(1, "KISS")

    def test_get_id(self):
        self.assertEqual(1, self.art.get_artist_id())

    def test_get_name(self):
        self.assertEqual("KISS", self.art.get_artist_name())

    def test_add_del_album(self):
        alb1 = classes.Album(1, "Creatures of the Night")
        alb2 = classes.Album(2, "The Best of Led Zeppelin")
        self.assertFalse(self.art.get_all_albums())
        self.art.add_album(alb1)
        self.assertListEqual([alb1], self.art.get_all_albums())
        self.art.add_album(alb2)
        self.assertListEqual([alb1, alb2], self.art.get_all_albums())
        self.art.delete_album(alb1)
        self.assertListEqual([alb2], self.art.get_all_albums())
        self.art.delete_album(alb2)
        self.assertFalse(self.art.get_all_albums())

    def test_add_del_single(self):
        single1 = classes.MusicComposition(2, "Escape", [(1, "Metallica")], [(1, "Ride the Lightning")], 1984, time(0, 3, 41), None, 1, [None])
        single2 = classes.MusicComposition(3, "Nightquest", [(2, "Nightwish")], [(2, "Oceanborn")], 1998, time(0, 4, 15), None, 2, [None])
        self.assertFalse(self.art.get_all_singles())
        self.art.add_single(single1)
        self.assertListEqual([single1], self.art.get_all_singles())
        self.art.add_single(single2)
        self.assertListEqual([single1, single2], self.art.get_all_singles())
        self.art.delete_single(single1)
        self.assertListEqual([single2], self.art.get_all_singles())
        self.art.delete_single(single2)
        self.assertFalse(self.art.get_all_singles())


class TestMusicComposition(unittest.TestCase):
    def setUp(self):
        self.mc = classes.MusicComposition(2, "Escape", [""], [""], 1984, time(0, 3, 41), None, 1, [None])

    def test_get_mc_id(self):
        self.assertEqual(2, self.mc.get_mc_id())

    def test_get_name(self):
        self.assertEqual("Escape", self.mc.get_name())

    def test_add_get_artist(self):
        art1 = (1, "Metallica")
        art2 = (2, "Scorpions")
        self.mc.add_artist(art1)
        self.mc.add_artist(art2)#не сработает так как уже есть запись в списке
        self.assertEqual( [art1] , self.mc.get_artist() )
        self.mc.delete_artist()
        self.assertFalse(self.mc.get_artist())
        self.mc.add_artist(art2)
        self.assertEqual([art2], self.mc.get_artist())

    def test_add_get_album(self):
        alb1 = (1, "Ride the Lightning")
        alb2 = (2, "Music from Elder")
        self.mc.add_album(alb1)
        self.mc.add_album(alb2)#не сработает так как уже есть запись в списке
        self.assertEqual([alb1], self.mc.get_album())
        self.mc.delete_album()
        self.assertFalse(self.mc.get_album())
        self.mc.add_album(alb2)
        self.assertEqual([alb2], self.mc.get_album())

    def test_get_year(self):
        self.assertEqual(1984, self.mc.get_year())

    def test_get_length(self):
        self.assertEqual(time(0, 3, 41), self.mc.get_length())

    def test_set_get_priority(self):
        self.assertEqual(1, self.mc.get_priority())
        self.mc.set_priority(2)
        self.assertEqual(2, self.mc.get_priority())

    def test_add_get_delete_genre(self):
        gen1 = (1, "Rock")
        gen2 = (2, "Pop")
        self.assertFalse(self.mc.get_all_genres())#проверяем что жанров нет
        self.mc.add_genre(gen1)
        self.assertListEqual([gen1], self.mc.get_all_genres())
        self.mc.add_genre(gen2)
        self.assertListEqual([gen1, gen2], self.mc.get_all_genres())
        self.mc.delete_genre(gen1)
        self.assertListEqual([gen2], self.mc.get_all_genres())
        self.mc.delete_genre(gen2)
        self.assertFalse(self.mc.get_all_genres())

    def test_add_delete_file_get_hesh(self):
        self.assertFalse(self.mc.get_hesh())
        self.mc.add_file("asdefdkfd")
        self.assertEqual("asdefdkfd", self.mc.get_hesh())
        self.mc.delete_file()
        self.assertFalse(self.mc.get_hesh())


class TestThematicPlaylist(unittest.TestCase):
    def setUp(self):
        self.tp = classes.ThematicPlaylist(1, "Best of 80's", "dummy description")

    def test_get_id(self):
        self.assertEqual(1, self.tp.get_tp_id())

    def test_get_playlist_name(self):
        self.assertEqual("Best of 80's", self.tp.get_playlist_name())

    def test_add_get_delete_composition(self):
        com1 = classes.MusicComposition(2, "Escape", [(1, "Metallica")], [(1, "Ride the Lightning")], 1984, time(0, 3, 41), None, 1, [None])
        com2 = classes.MusicComposition(3, "Nightquest", [(2, "Nightwish")], [(2, "Oceanborn")], 1998, time(0, 4, 15), None, 2, [None])
        self.assertFalse(self.tp.get_all_compositions())
        self.tp.add_composition(com1)
        self.assertListEqual([com1], self.tp.get_all_compositions())
        self.tp.add_composition(com2)
        self.assertListEqual([com1, com2], self.tp.get_all_compositions())
        self.tp.delete_composition(com1)
        self.assertListEqual([com2], self.tp.get_all_compositions())
        self.tp.delete_composition(com2)
        self.assertFalse(self.tp.get_all_compositions())


if __name__ == '__main__':
    unittest.main()
