import tkinter as tk
from tkinter import *
from tkinter import messagebox
from tkinter import ttk
from tkinter import filedialog as fd
import componenta


class GenericError(Exception):
    def __init__(self, message):
        super().__init__(message)


# Авторизация
class Main(tk.Frame):
    def __init__(self, root):
        super().__init__(root)
        self.init_main()

    def init_main(self):
        MainSystemWindow()#проверка пока отключена
        self.login = tk.StringVar()
        self.password = tk.StringVar()

        self.label_1 = tk.Label(root, text="Ведите логин: ", fg="black", width=30).place(x=30, y=50)
        self.entry_2 = ttk.Entry(root, textvar=self.login, width=30).place(x=180, y=50)

        self.label_2 = tk.Label(root, text="Ведите пароль: ", fg="black", width=30).place(x=25, y=80)
        self.entry_2 = ttk.Entry(root, textvar=self.password, width=30).place(x=180, y=80)

        self.btn_1 = tk.Button(root, text="Войти", width="20", command=self.validate, background="royalblue",
                               foreground="white", font="15").place(x=120, y=130)

    def validate(self):
        self.log = self.login.get()
        self.pas = self.password.get()
        cursor = db.Validate(self.log)
        self.users = cursor.fetchone()
        print(self.users)
        if self.users and self.users[2] == self.pas:
            MainSystemWindow()
            cursor.close()
        else:
            messagebox.showinfo("Error", "Неверные данные")
            cursor.close()


class MainSystemWindow(tk.Toplevel):
    def __init__(self):
        super().__init__()

        self.title("Admin Panel")
        self.geometry("750x720")
        surface = ttk.Frame(self)
        surface.pack(expand=True, fill=BOTH)
        self.labelartist = tk.Label(surface, text="Выберите Исполнителя").place(x=10, y=10)
        self.artist_list = ttk.Combobox(surface, text="Выберите Артиста", values=[], width=50)  # для артистов
        cursor = db.GetArtistsList()# дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.artist_list['values'] += (record[i][1],)
        cursor.close()

        self.artist_list.place(x=10, y=35)
        self.artist_list.bind("<<ComboboxSelected>>", self.LoadAlbums)

        self.labelalbum = tk.Label(surface, text="Выберите Альбом исполнителя").place(x=400, y=10)
        self.albums_list = ttk.Combobox(surface, text="Выберите Альбом", state="normal", values=[], width=50)
        self.var = tk.IntVar()
        self.singles = ttk.Checkbutton(surface, text="Режим синглов", command=self.IsSingles, variable=self.var)
        self.albums_list.bind("<<ComboboxSelected>>", self.LoadCompositions)

        # Менюшка БД
        self.button10 = tk.Button(surface, text="Новый Исполнитель", width="17", height="1", command=self.NewArtist,
                                  background="dark green", foreground="white", font="Calibri 10").place(x=10, y=420)
        self.button11 = tk.Button(surface, text="Новый Альбом", width="17", height="1", command=self.NewAlbum,
                                  background="gray", foreground="white", font="Calibri 10").place(x=10, y=460)
        self.button12 = tk.Button(surface, text="Новая Композиция", width="17", height="1", command=self.NewMC,
                                  background="gray", foreground="white", font="Calibri 10").place(x=10, y=500)
        self.button13 = tk.Button(surface, text="Новый Жанр", width="17", height="1", command=self.CreateNewGenre,
                                  background="gray", foreground="white", font="Calibri 10").place(x=10, y=540)
        self.button14 = tk.Button(surface, text="Новый Сингл", width="17", height="1", command=self.NewSingle,
                                  background="gray", foreground="white", font="Calibri 10").place(x=10, y=580)
        self.button15 = tk.Button(surface, text="Удалить Исполнителя", width="17", height="1",
                                  command=self.DeleteArtist, background="dark red", foreground="white",
                                  font="Calibri 10").place(x=145, y=420)
        self.button16 = tk.Button(surface, text="Удалить Альбом", width="17", height="1", command=self.DeleteAlbum,
                                  background="gray", foreground="white", font="Calibri 10").place(x=145, y=460)
        self.button17 = tk.Button(surface, text="Удалить Композицию", width="17", height="1", command=self.DeleteMC,
                                  background="gray", foreground="white", font="Calibri 10").place(x=145, y=500)
        self.button27 = tk.Button(surface, text="Удалить Жанр", width="17", height="1", command=self.DeleteGenreFromDb,
                                  background="gray", foreground="white", font="Calibri 10").place(x=145, y=540)
        self.button18 = tk.Button(surface, text="Удалить Сингл", width="17", height="1",
                                  command=self.DeleteSingleFromDB,
                                  background="gray", foreground="white", font="Calibri 10").place(x=145, y=580)
        # Менюшка БД Конец

        # Менюшка изменения полей
        self.button19 = tk.Button(surface, text="Изменить имя жанра", width="23", height="1",
                                  command=self.ChangeGenreName, background="yellow", font="Calibri 10").place(x=285,
                                                                                                              y=540)
        self.button20 = tk.Button(surface, text="Изменить имя исполнителя", width="23", height="1",
                                  command=self.ChangeArtistName, background="yellow", font="Calibri 10").place(x=285,
                                                                                                               y=420)
        self.button21 = tk.Button(surface, text="Изменить название альбома", width="23", height="1",
                                  command=self.ChangeAlbumTitle, background="yellow", font="Calibri 10").place(x=285,
                                                                                                               y=460)
        # Менюшка изменения полей Конец

        # Менюшка файла
        self.button22 = tk.Button(surface, text="Изменить приоритет", width="17", height="1",
                                  command=self.ChangeMCPriority, background="yellow", font="Calibri 10").place(x=465,
                                                                                                               y=420)
        self.button23 = tk.Button(surface, text="Добавить файл", width="17", height="1", command=self.AddFile,
                                  background="green", foreground="white", font="Calibri 10").place(x=465, y=460)
        self.button24 = tk.Button(surface, text="Удалить файл", width="17", height="1", command=self.DeleteFile,
                                  background="red", foreground="white", font="Calibri 10").place(x=465, y=500)
        # менюшка файла конец

        # правая менюшка
        self.button25 = tk.Button(surface, text="Добавить Жанр", width="15", height="1",
                                  command=self.AddCurrentGenreToMC, background="green", foreground="white",
                                  font="Calibri 10").place(x=605, y=420)
        self.button26 = tk.Button(surface, text="Удалить Жанр", width="15", height="1",
                                  command=self.DeleteCurrentGenreFromMC, background="red", foreground="white",
                                  font="Calibri 10").place(x=605, y=460)
        # правая менюшка конец

        # Нижняя менюшка
        self.button28 = tk.Button(surface, text="Добавить альбом исполнителю", width="25", height="1",
                                  command=self.AddAlbumToArtist, background="green", foreground="white",
                                  font="Colibri 10").place(x=10, y=630)
        self.button29 = tk.Button(surface, text="Добавить сингл исполнителю", width="25", height="1",
                                  command=self.AddSingleToArtist, background="green", foreground="white",
                                  font="Colibri 10").place(x=230, y=630)
        self.button30 = tk.Button(surface, text="Добавить композицию в альбом", width="25", height="1",
                                  command=self.AddMCinAlbum, background="green", foreground="white",
                                  font="Colibri 10").place(x=450, y=630)

        self.button31 = tk.Button(surface, text="Удалить альбом исполнителя", width="25", height="1",
                                  command=self.DeleteAlbumFromArtist, background="red", foreground="white",
                                  font="Colibri 10").place(x=10, y=670)
        self.button32 = tk.Button(surface, text="Удалить сингл исполнителя", width="25", height="1",
                                  command=self.DeleteSingle, background="red", foreground="white",
                                  font="Colibri 10").place(x=230, y=670)
        self.button33 = tk.Button(surface, text="Удалить композицию из альбома", width="25", height="1",
                                  command=self.DeleteMCFromAlbum, background="red", foreground="white",
                                  font="Colibri 10").place(x=450, y=670)
        # Нижняя менюшка конец

        self.button666 = tk.Button(surface, text="Выход", width="7", height="3", command=self.kill_app,
                                   background="black", foreground="white", font="Colibri 10").place(x=680, y=630)
        self.mc_tree = ttk.Treeview(surface, columns=('no', 'name', 'year', 'length', 'hash', 'priority'), height=15,
                                    show='headings')

        self.genres_tree = ttk.Treeview(surface, columns='genres', height=15, show='headings')
        self.genres_tree.column('genres', width=120)
        self.genres_tree.heading('genres', text="Жанры")
        self.genres_tree.place(x=600, y=80)

        self.mc_tree.column('no', width=30)
        self.mc_tree.column('name', width=120)
        self.mc_tree.column('year', width=100)
        self.mc_tree.column('length', width=130)
        self.mc_tree.column('hash', width=100)
        self.mc_tree.column('priority', width=100)

        self.mc_tree.heading('no', text='№')
        self.mc_tree.heading('name', text='Имя композиции')
        self.mc_tree.heading('year', text='Год')
        self.mc_tree.heading('length', text='Продолжительность')
        self.mc_tree.heading('hash', text='Хэш')
        self.mc_tree.heading('priority', text='Приоритет')

        self.mc_tree.place(x=10, y=80)
        self.mc_tree.bind('<<TreeviewSelect>>', self.ClickOnCompositionInMC_Tree)

    def NewAlbum(self):
        self.AddNewAlbumWindow = tk.Toplevel(app)
        self.AddNewAlbumWindow.title("Add Album")
        self.AddNewAlbumWindow.geometry("190x120")
        self.labelalbum = tk.Label(self.AddNewAlbumWindow, text="Введите новый альбом").place(x=10, y=10)
        self.new_album = tk.StringVar()
        self.new_album_val = tk.Entry(self.AddNewAlbumWindow, state="normal", textvar=self.new_album,
                                      width=22).place(x=10, y=30)
        self.button50 = tk.Button(self.AddNewAlbumWindow, text="Добавить альбом", width="17", height="1",
                                  command=self.CommitNewAlbum, font="Colibri 10").place(x=15, y=70)

    def CommitNewAlbum(self):
        data = self.new_album.get()
        db.CommitNewAlbum(str(data))#дергаем компоненту
        self.LoadAlbums(None)
        self.AddNewAlbumWindow.destroy()

    def DeleteAlbum(self):
        self.DeleteAlbumWindow = tk.Toplevel(app)
        self.DeleteAlbumWindow.title("Delete Artist")
        self.DeleteAlbumWindow.geometry("170x120")
        self.labelartist = tk.Label(self.DeleteAlbumWindow, text="Выберите альбом").place(x=10, y=10)
        self.cur_album = ttk.Combobox(self.DeleteAlbumWindow, values=[], width=20)
        self.button50 = tk.Button(self.DeleteAlbumWindow, text="Удалить альбом", width="17", height="1",
                                  command=self.CommitDeleteAlbum, font="Colibri 10").place(x=15, y=70)
        cursor = db.GetAlbumsList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.cur_album['values'] += (record[i][1],)
        cursor.close()
        self.cur_album.place(x=10, y=40)

    def CommitDeleteAlbum(self):
        chosen_album = self.cur_album.get()
        db.CommitDeleteAlbum(chosen_album)#дергаем компоненту
        self.LoadAlbums(None)
        self.DeleteAlbumWindow.destroy()

    def NewMC(self):
        pass

    def NewSingle(self):
        pass

    def DeleteMC(self):
        self.DeleteMCWindow = tk.Toplevel(app)
        self.DeleteMCWindow.title("Delete Composition")
        self.DeleteMCWindow.geometry("170x120")
        self.labelmc = tk.Label(self.DeleteMCWindow, text="Выберите композицию").place(x=10, y=10)
        self.cur_mc = ttk.Combobox(self.DeleteMCWindow, values=[], width=20)
        self.button50 = tk.Button(self.DeleteMCWindow, text="Удалить композицию", width="17", height="1",
                                  command=self.CommitDeleteMC, font="Colibri 10").place(x=15, y=70)
        cursor = db.GetAllMC()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.cur_mc['values'] += (record[i][2],)
        cursor.close()
        self.cur_mc.place(x=10, y=40)

    def CommitDeleteMC(self):
        chosen_mc = self.cur_mc.get()
        db.CommitDeleteMC(chosen_mc)#дергаем компоненту
        self.DeleteMCWindow.destroy()

    def DeleteSingleFromDB(self):
        self.DeleteSingleWindow = tk.Toplevel(app)
        self.DeleteSingleWindow.title("Delete Composition")
        self.DeleteSingleWindow.geometry("170x120")
        self.labelmc = tk.Label(self.DeleteSingleWindow, text="Выберите композицию").place(x=10, y=10)
        self.cur_mc = ttk.Combobox(self.DeleteSingleWindow, values=[], width=20)
        self.button50 = tk.Button(self.DeleteSingleWindow, text="Удалить композицию", width="17", height="1",
                                  command=self.CommitDeleteSingle, font="Colibri 10").place(x=15, y=70)
        cursor = db.GetAllAllSingles()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.cur_mc['values'] += (record[i][2],)
        cursor.close()
        self.cur_mc.place(x=10, y=40)

    def CommitDeleteSingle(self):
        chosen_mc = self.cur_mc.get()
        db.CommitDeleteSingle(chosen_mc)#дергаем компоненту
        self.DeleteSingleWindow.destroy()

    def NewArtist(self):
        self.AddNewArtistWindow = tk.Toplevel(app)
        self.AddNewArtistWindow.title("Add Artist")
        self.AddNewArtistWindow.geometry("190x120")
        self.labelartist = tk.Label(self.AddNewArtistWindow, text="Введите нового исполнителя").place(x=10, y=10)
        self.new_artist = tk.StringVar()
        self.new_album_val = tk.Entry(self.AddNewArtistWindow, state="normal", textvar=self.new_artist,
                                      width=22).place(x=10, y=30)
        self.button50 = tk.Button(self.AddNewArtistWindow, text="Добавить исполнителя", width="17", height="1",
                                  command=self.CommitNewArtist, font="Colibri 10").place(x=15, y=70)

    def CommitNewArtist(self):
        data = self.new_artist.get()
        query_data = str(data)
        U_case.addNewArtist(query_data)
        self.artist_list.place_forget()
        self.artist_list.set('')
        self.artist_list['values'] = ''
        cursor = db.GetArtistsList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.artist_list['values'] += (record[i][1],)
        cursor.close()
        self.artist_list.place(x=10, y=35)
        self.AddNewArtistWindow.destroy()

    def DeleteArtist(self):
        self.DeleteArtistWindow = tk.Toplevel(app)
        self.DeleteArtistWindow.title("Delete Artist")
        self.DeleteArtistWindow.geometry("170x120")
        self.labelartist = tk.Label(self.DeleteArtistWindow, text="Выберите исполнителя").place(x=10, y=10)
        self.cur_artist = ttk.Combobox(self.DeleteArtistWindow, values=[], width=20)
        self.button50 = tk.Button(self.DeleteArtistWindow, text="Удалить исполнителя", width="17", height="1",
                                  command=self.CommitDeleteArtistWindow, font="Colibri 10").place(x=15, y=70)
        cursor = db.GetArtistsList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.cur_artist['values'] += (record[i][1],)
        cursor.close()
        self.cur_artist.place(x=10, y=40)

    def CommitDeleteArtistWindow(self):
        chosen_artist = self.cur_artist.get()
        db.CommitDeleteArtist(chosen_artist)#дергаем компоненту

        self.artist_list.place_forget()
        self.artist_list.set('')
        self.artist_list['values'] = ''
        cursor = db.GetArtistsList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.artist_list['values'] += (record[i][1],)
        cursor.close()
        self.artist_list.place(x=10, y=35)
        self.DeleteArtistWindow.destroy()

    def AddMCinAlbum(self):
        self.AddMCinAlbumWindow = tk.Toplevel(app)
        self.AddMCinAlbumWindow.title("Add Album")
        self.AddMCinAlbumWindow.geometry("250x250")
        self.labelmc = tk.Label(self.AddMCinAlbumWindow, text="Выберите существующую композицию").place(x=10, y=10)
        self.mc_list = ttk.Combobox(self.AddMCinAlbumWindow, values=[" ", ], width=20)
        self.labelmc2 = tk.Label(self.AddMCinAlbumWindow, text="Или добавьте новую").place(x=10, y=70)
        self.labelmc3 = tk.Label(self.AddMCinAlbumWindow, text="Имя композиции").place(x=150, y=100)
        self.new_mc_name_var = tk.StringVar()
        self.new_mc_name = tk.Entry(self.AddMCinAlbumWindow, state="normal", textvar=self.new_mc_name_var,
                                    width=22).place(x=10, y=100)
        self.labelsingle4 = tk.Label(self.AddMCinAlbumWindow, text="Год").place(x=150, y=130)
        self.new_mc_year_var = tk.StringVar()
        self.new_mc_year = tk.Entry(self.AddMCinAlbumWindow, state="normal", textvar=self.new_mc_year_var,
                                    width=22).place(x=10, y=130)
        self.labelmc5 = tk.Label(self.AddMCinAlbumWindow, text="Приоритет").place(x=150, y=160)
        self.new_mc_priority_var = tk.StringVar()
        self.new_mc_priority = tk.Entry(self.AddMCinAlbumWindow, state="normal", textvar=self.new_mc_priority_var,
                                        width=22).place(x=10, y=160)
        self.button50 = tk.Button(self.AddMCinAlbumWindow, text="Добавить композицию", width="15", height="1",
                                  command=self.CommitAddMCinAlbum, font="Colibri 10").place(x=15, y=200)
        cursor = db.GetAllMC()
        record = cursor.fetchall()#дергаем компоненту
        for i in range(len(record)):
            self.mc_list['values'] += (record[i][2],)
        cursor.close()
        self.mc_list.place(x=10, y=40)

    def CommitAddMCinAlbum(self):
        status = self.mc_list.get()
        album = self.albums_list.get()
        if len(status) != 0:
            db.CommitAddMCinAlbum(status, album)#дергаем компоненту
            self.LoadCompositions(None)
            self.AddMCinAlbumWindow.destroy()
        else:  # добавление новой композиции в альбом
            new_mc_name_var_data = self.new_mc_name_var.get()
            new_mc_year_var_data = self.new_mc_year_var.get()
            new_mc_priority_var_data = self.new_mc_priority_var.get()
            U_case.AddNewMCinAlbum(album, str(new_mc_name_var_data), str(new_mc_year_var_data), str(new_mc_priority_var_data))#дергаем компоненту
            self.LoadCompositions(None)
            self.AddMCinAlbumWindow.destroy()

    def AddSingleToArtist(self):
        self.AddSingleToArtistWindow = tk.Toplevel(app)
        self.AddSingleToArtistWindow.title("Add Single")
        self.AddSingleToArtistWindow.geometry("250x250")
        self.labelsingle = tk.Label(self.AddSingleToArtistWindow, text="Выберите существующий сингл").place(x=10, y=10)
        self.single_list = ttk.Combobox(self.AddSingleToArtistWindow, values=[" ", ], width=20)
        self.labelsingle2 = tk.Label(self.AddSingleToArtistWindow, text="Или добавьте новый сингл").place(x=10, y=70)
        self.labelsingle3 = tk.Label(self.AddSingleToArtistWindow, text="Имя композиции").place(x=150, y=100)
        self.new_single_name_var = tk.StringVar()
        self.new_single_name = tk.Entry(self.AddSingleToArtistWindow, state="normal", textvar=self.new_single_name_var,
                                        width=22).place(x=10, y=100)
        self.labelsingle4 = tk.Label(self.AddSingleToArtistWindow, text="Год").place(x=150, y=130)
        self.new_single_year_var = tk.StringVar()
        self.new_single_year = tk.Entry(self.AddSingleToArtistWindow, state="normal", textvar=self.new_single_year_var,
                                        width=22).place(x=10, y=130)
        self.labelsingle5 = tk.Label(self.AddSingleToArtistWindow, text="Приоритет").place(x=150, y=160)
        self.new_single_priority_var = tk.StringVar()
        self.new_single_priority = tk.Entry(self.AddSingleToArtistWindow, state="normal",
                                            textvar=self.new_single_priority_var,
                                            width=22).place(x=10, y=160)
        self.button50 = tk.Button(self.AddSingleToArtistWindow, text="Добавить сингл", width="15", height="1",
                                  command=self.CommitAddSingleToArtist, font="Colibri 10").place(x=15, y=200)
        cursor = db.GetAllSingles()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.single_list['values'] += (record[i][1],)
        cursor.close()
        self.single_list.place(x=10, y=40)

    def CommitAddSingleToArtist(self):
        status = self.single_list.get()
        artist = self.artist_list.get()
        if len(status) != 0:
            db.CommitAddSingleToArtist(artist, status)#дергаем компоненту
            self.IsSingles()
            self.AddSingleToArtistWindow.destroy()
        else:
            new_single_name_var_data = self.new_single_name_var.get()
            new_single_year_var_data = self.new_single_year_var.get()
            new_single_priority_var_data = self.new_single_priority_var.get()
            U_case.AddNewSingleToArtist(artist, str(new_single_name_var_data), str(new_single_year_var_data), str(new_single_priority_var_data))#дергаем компоненту
            self.IsSingles()
            self.AddSingleToArtistWindow.destroy()

    def AddAlbumToArtist(self):
        self.AddAlbumToArtistWindow = tk.Toplevel(app)
        self.AddAlbumToArtistWindow.title("Add Album")
        self.AddAlbumToArtistWindow.geometry("250x250")
        self.labelalbum = tk.Label(self.AddAlbumToArtistWindow, text="Выберите существующий альбом").place(x=10, y=10)
        self.album_list = ttk.Combobox(self.AddAlbumToArtistWindow, values=["''", ], width=20)
        self.labelalbum2 = tk.Label(self.AddAlbumToArtistWindow, text="Или добавьте новый альбом").place(x=10, y=70)
        self.new_album = tk.StringVar()
        self.new_album_val = tk.Entry(self.AddAlbumToArtistWindow, state="normal", textvar=self.new_album,
                                      width=22).place(x=10, y=100)
        self.button50 = tk.Button(self.AddAlbumToArtistWindow, text="Добавить альбом", width="15", height="1",
                                  command=self.CommitAddAlbumToArtist, font="Colibri 10").place(x=15, y=150)
        cursor = db.GetAlbumsList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.album_list['values'] += (record[i][1],)
        cursor.close()
        self.album_list.place(x=10, y=40)

    def CommitAddAlbumToArtist(self):
        status = self.album_list.get()
        artist = self.artist_list.get()
        if len(status) != 0:
            current_id = db.GetAlbum(status)#дергаем компоненту
            record_album = current_id.fetchall()
            current_id = db.GetArtist(artist)#дергаем компоненту
            record_artist = current_id.fetchall()
            db.CommitAddAlbumToArtist(record_album, record_artist)#дергаем компоненту
            self.LoadAlbums(None)
            self.AddAlbumToArtistWindow.destroy()
        else:
            data = self.new_album.get()
            U_case.addNewAlbumToArtist(artist, str(data))#дергаем компоненту
            self.LoadAlbums(None)
            self.AddAlbumToArtistWindow.destroy()

    def DeleteAlbumFromArtist(self):
        chosen_artist = self.artist_list.get()
        chosen_album = self.albums_list.get()
        U_case.DeleteAlbumFromArtist(chosen_artist, chosen_album)#дергаем компоненту
        self.LoadAlbums(None)

    def DeleteMCFromAlbum(self):
        item = self.mc_tree.focus()
        item_inner = self.mc_tree.item(item)
        chosen_mc = item_inner.get("values")
        chosen_album = self.albums_list.get()
        U_case.DeleteCompositionFromAlbum(chosen_album, chosen_mc[0])#дергаем компоненту
        self.LoadCompositions(None)

    def DeleteSingle(self):
        chosen_artist = self.artist_list.get()
        item = self.mc_tree.focus()
        item_inner = self.mc_tree.item(item)
        chosen_single = item_inner.get("values")
        U_case.DeleteSingleFromArtist(chosen_artist, chosen_single[0])#дергаем компоненту
        self.IsSingles()

    def AddFile(self):
        file_name = fd.askopenfilename()
        item = self.mc_tree.focus()
        item_inner = self.mc_tree.item(item)
        chosen_mc = item_inner.get("values")
        U_case.AddFileToComposition(chosen_mc[0], file_name)#дергаем компоненту
        status = self.var.get()
        if status == 0:
            self.LoadCompositions(None)
        else:
            self.IsSingles()

    def DeleteFile(self):
        item = self.mc_tree.focus()
        item_inner = self.mc_tree.item(item)
        chosen_mc = item_inner.get("values")
        U_case.DeleteFileFromComposition(chosen_mc[0])  # дергаем компоненту
        status = self.var.get()
        if status == 0:
            self.LoadCompositions(None)
        else:
            self.IsSingles()

    def ChangeMCPriority(self):
        self.ChangeMCPriorityWindow = tk.Toplevel(app)
        self.ChangeMCPriorityWindow.title("Re-priority")
        self.ChangeMCPriorityWindow.geometry("180x100")
        self.labelgenre = tk.Label(self.ChangeMCPriorityWindow, text="Введите новый приоритет").place(x=10, y=10)
        self.priority = tk.StringVar()
        self.priority_val = ttk.Entry(self.ChangeMCPriorityWindow, textvar=self.priority, width=22).place(x=10, y=30)
        self.button50 = tk.Button(self.ChangeMCPriorityWindow, text="Изменить приоритет", width="15", height="1",
                                  command=self.CommitChangeMCPriority, font="Colibri 10").place(x=15, y=60)

    def CommitChangeMCPriority(self):
        item = self.mc_tree.focus()
        item_inner = self.mc_tree.item(item)
        chosen_mc = item_inner.get("values")
        data = self.priority.get()
        new_priority = str(data)
        U_case.ChangeCompositionPriority(chosen_mc[0], new_priority)#дергаем компоненту
        status = self.var.get()
        if status == 0:
            self.LoadCompositions(None)
        else:
            self.IsSingles()
        self.ChangeMCPriorityWindow.destroy()

    def ChangeGenreName(self):
        self.ChangeGenreNameWindow = tk.Toplevel(app)
        self.ChangeGenreNameWindow.title("Re-name Album")
        self.ChangeGenreNameWindow.geometry("220x150")
        self.labelgenre = tk.Label(self.ChangeGenreNameWindow, text="Выберите Жанр").place(x=10, y=10)
        self.genre_list = ttk.Combobox(self.ChangeGenreNameWindow, values=[], width=20)
        self.labelname = tk.Label(self.ChangeGenreNameWindow, text="Введите новое имя жанра").place(x=10, y=60)
        self.genre = tk.StringVar()
        self.genre_name = ttk.Entry(self.ChangeGenreNameWindow, textvar=self.genre, width=22).place(x=10, y=80)
        self.button50 = tk.Button(self.ChangeGenreNameWindow, text="Изменить Имя", width="15", height="1",
                                  command=self.CommitChangeGenreName, font="Colibri 10").place(x=15, y=110)
        cursor = db.GetGenresList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.genre_list['values'] += (record[i][1],)
        cursor.close()
        self.genre_list.place(x=10, y=40)

    def CommitChangeGenreName(self):
        chosen_genre = self.genre_list.get()
        data = self.genre.get()
        query_data = str(data)
        U_case.ChangeGenreName(chosen_genre, query_data) #дергаем компоненту
        self.ChangeGenreNameWindow.destroy()

    def ChangeAlbumTitle(self):
        self.ChangeAlbumTitleWindow = tk.Toplevel(app)
        self.ChangeAlbumTitleWindow.title("Re-name Album")
        self.ChangeAlbumTitleWindow.geometry("220x150")
        self.labelalbum = tk.Label(self.ChangeAlbumTitleWindow, text="Выберите Альбом").place(x=10, y=10)
        self.alb_list = ttk.Combobox(self.ChangeAlbumTitleWindow, values=[], width=20)
        self.labelname = tk.Label(self.ChangeAlbumTitleWindow, text="Введите новое название альбома").place(x=10, y=60)
        self.album = tk.StringVar()
        self.album_title = ttk.Entry(self.ChangeAlbumTitleWindow, textvar=self.album, width=22).place(x=10, y=80)
        self.button50 = tk.Button(self.ChangeAlbumTitleWindow, text="Изменить Название", width="15", height="1",
                                  command=self.CommitChangeAlbumTitle, font="Colibri 10").place(x=15, y=110)
        cursor = db.GetAlbumsList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.alb_list['values'] += (record[i][1],)
        cursor.close()
        self.alb_list.place(x=10, y=40)

    def CommitChangeAlbumTitle(self):
        chosen_album = self.alb_list.get()
        data = self.album.get()
        query_data = str(data)
        U_case.ChangeAlbumTitle(chosen_album, query_data)#дергаем компоненту
        self.LoadAlbums(None)
        self.ChangeAlbumTitleWindow.destroy()

    def ChangeArtistName(self):
        self.ChangeArtistNameWindow = tk.Toplevel(app)
        self.ChangeArtistNameWindow.title("Re-name Artist")
        self.ChangeArtistNameWindow.geometry("220x150")
        self.labelartist = tk.Label(self.ChangeArtistNameWindow, text="Выберите Исполнителя").place(x=10, y=10)
        self.art_list = ttk.Combobox(self.ChangeArtistNameWindow, values=[], width=20)
        self.labelname = tk.Label(self.ChangeArtistNameWindow, text="Введите новое имя исполнителя").place(x=10, y=60)
        self.artist = tk.StringVar()
        self.artist_name = ttk.Entry(self.ChangeArtistNameWindow, textvar=self.artist, width=22).place(x=10, y=80)
        self.button50 = tk.Button(self.ChangeArtistNameWindow, text="Изменить Имя", width="15", height="1",
                                  command=self.CommitChangeArtistName, font="Colibri 10").place(x=15, y=110)
        cursor = db.GetArtistsList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.art_list['values'] += (record[i][1],)
        cursor.close()

        self.art_list.place(x=10, y=40)

    def CommitChangeArtistName(self):
        chosen_artist = self.art_list.get()
        data = self.artist.get()
        query_data = str(data)
        U_case.ChangeArtistName(chosen_artist, query_data)#дергаем компоненту
        self.artist_list.place_forget()
        self.artist_list.set('')
        self.artist_list['values'] = ''
        cursor = db.GetArtistsList()  # дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.artist_list['values'] += (record[i][1],)
        cursor.close()
        self.artist_list.place(x=10, y=35)
        self.ChangeArtistNameWindow.destroy()

    def CreateNewGenre(self):
        self.NewGenreWindow = tk.Toplevel(app)
        self.NewGenreWindow.title("Add Genre")
        self.NewGenreWindow.geometry("165x100")
        self.genre = tk.StringVar()
        self.labelgenre = tk.Label(self.NewGenreWindow, text="Введите новый Жанр").place(x=10, y=10)
        self.genre_enty = ttk.Entry(self.NewGenreWindow, textvar=self.genre, width=22).place(x=10, y=40)
        self.button50 = tk.Button(self.NewGenreWindow, text="Добавить Жанр", width="15", height="1",
                                  command=self.CommitCreateNewGenre, font="Colibri 10").place(x=15, y=70)

    def CommitCreateNewGenre(self):
        data = self.genre.get()
        db.CommitCreateNewGenre(str(data))#дергаем компоненту
        self.NewGenreWindow.destroy()

    def DeleteGenreFromDb(self):
        self.DeleteGenreWindow = tk.Toplevel(app)
        self.DeleteGenreWindow.title("Delete Genre")
        self.DeleteGenreWindow.geometry("165x100")
        self.labelgenre = tk.Label(self.DeleteGenreWindow, text="Выберите Жанр").place(x=10, y=10)
        self.genres_list = ttk.Combobox(self.DeleteGenreWindow, values=[], width=20)
        self.button50 = tk.Button(self.DeleteGenreWindow, text="Удалить Жанр", width="15", height="1",
                                  command=self.CommitDeleteGenreFromDb,
                                  font="Colibri 10").place(x=15, y=70)
        cursor = db.GetGenresList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.genres_list['values'] += (record[i][1],)
        cursor.close()
        self.genres_list.place(x=10, y=40)

    def CommitDeleteGenreFromDb(self):
        chosen_genre = self.genres_list.get()
        db.CommitDeleteGenreFromDb(chosen_genre)#дергаем компоненту
        self.DeleteGenreWindow.destroy()

    def kill_app(self):  # кнопка выхода
        root.destroy()

    def AddCurrentGenreToMC(self):
        self.GenreWindow = tk.Toplevel(app)
        self.GenreWindow.title("Add Genre")
        self.GenreWindow.geometry("165x100")
        self.labelgenre = tk.Label(self.GenreWindow, text="Выберите Жанр").place(x=10, y=10)
        self.genres_list = ttk.Combobox(self.GenreWindow, values=[], width=20)
        self.button50 = tk.Button(self.GenreWindow, text="Добавить Жанр", width="15", height="1",
                                  command=self.CommitAddCurrentGenreToMC, font="Colibri 10").place(x=15, y=70)
        cursor = db.GetGenresList()#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.genres_list['values'] += (record[i][1],)
        cursor.close()
        self.genres_list.place(x=10, y=40)

    def CommitAddCurrentGenreToMC(self):
        chosen_genre = self.genres_list.get()
        item = self.mc_tree.focus()
        item_inner = self.mc_tree.item(item)
        chosen_mc = item_inner.get("values")
        U_case.addCurrentGenreToMC(chosen_mc[0], chosen_genre) #дергаем компоненту
        self.GenreWindow.destroy()
        self.ClickOnCompositionInMC_Tree(None)

    def DeleteCurrentGenreFromMC(self):
        item = self.genres_tree.focus()
        item_inner = self.genres_tree.item(item)
        chosen_genre = item_inner.get("values")
        item = self.mc_tree.focus()
        item_inner = self.mc_tree.item(item)
        chosen_mc = item_inner.get("values")
        U_case.DeleteGenreFromComposition(chosen_mc[0], chosen_genre[0]) #дергаем компоненту
        self.ClickOnCompositionInMC_Tree(None)

    def LoadAlbums(self, event):
        # self.labelalbum.place(x=400, y=10)
        self.singles.place(x=200, y=10)
        for row in self.genres_tree.get_children():
            self.genres_tree.delete(row)
        for row in self.mc_tree.get_children():
            self.mc_tree.delete(row)
        self.albums_list.place_forget()
        self.albums_list.set('')
        self.albums_list['values'] = ''
        self.chosen_artist = self.artist_list.get()
        cursor = db.GetArtistAlbums(self.chosen_artist)#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.albums_list['values'] += (record[i][1],)
        cursor.close()
        #connection.close()
        self.albums_list.place(x=400, y=35)

    def LoadCompositions(self, event):
        self.mc_tree.place_forget()
        for row in self.genres_tree.get_children():
            self.genres_tree.delete(row)
        for row in self.mc_tree.get_children():
            self.mc_tree.delete(row)
        chosen_artist = self.artist_list.get()
        chosen_album = self.albums_list.get()
        cursor = db.GetMCList(chosen_album)#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.mc_tree.insert('', 'end', values=(
                record[i][0], record[i][1], record[i][2], record[i][3], record[i][4], record[i][5], ""))
        cursor.close()
        self.mc_tree.place(x=10, y=80)

    def IsSingles(self):
        status = self.var.get()
        if status == 1:
            chosen_artist = self.artist_list.get()
            self.albums_list['state'] = "disabled"
            self.mc_tree.place_forget()
            for row in self.genres_tree.get_children():
                self.genres_tree.delete(row)
            for row in self.mc_tree.get_children():
                self.mc_tree.delete(row)
            cursor = db.GetSinglesList(chosen_artist)#дергаем компоненту
            record = cursor.fetchall()
            for i in range(len(record)):
                self.mc_tree.insert('', 'end', values=(
                    record[i][0], record[i][1], record[i][2], record[i][3], record[i][4], record[i][5], ""))
            self.mc_tree.place(x=10, y=80)
            cursor.close()
        if status == 0:
            self.albums_list['state'] = "normal"
            for row in self.genres_tree.get_children():
                self.genres_tree.delete(row)
            for row in self.mc_tree.get_children():
                self.mc_tree.delete(row)

    def ClickOnCompositionInMC_Tree(self, event):
        self.genres_tree.place_forget()
        for row in self.genres_tree.get_children():
            self.genres_tree.delete(row)
        item = self.mc_tree.focus()
        item_inner = self.mc_tree.item(item)
        com_id = item_inner.get("values")
        cursor = db.GetCompositionGenre(com_id)#дергаем компоненту
        record = cursor.fetchall()
        for i in range(len(record)):
            self.genres_tree.insert('', 'end', values=(record[i][0], ""))
        cursor.close()
        self.genres_tree.place(x=600, y=80)


if __name__ == "__main__":
    # экземпляры классов
    db = componenta.Storage()
    black_box = componenta.BB()
    U_case = componenta.UseCase(db, black_box)

    root = tk.Tk()
    root.title("Radiostation")
    root.geometry("400x200")
    app = Main(root)
    app.pack()
    root.mainloop()
