import unittest
from datetime import time

import componenta
from classes import MusicComposition, Artist, Album, Genres

db = componenta.Storage()
black_box = componenta.BB()


class BlackBoxTest:
    def SaveFileToBlackBox(self, file):
        hesh = hash(file)
        return hesh

    def DeleteFileFromBlackBox(self, hash):
        pass


class AddNewMCDbSpy:
    def __init__(self):
        self.saved_mc = None
        self.saved_album = None
        self.saved_artist = None
        self.saved_genre = None
        self.album = Album(
            album_id=1,
            title="Album # 1"
        )

        self.album_mc = Album(
            album_id=2,
            title="Album # 1"
        )

        self.artist = Artist(
            artist_id=1,
            artist_name="Kiss"
        )

        self.artist_single = Artist(
            artist_id=3,
            artist_name="Kiss"
        )

        self.artist_alb = Artist(
            artist_id=2,
            artist_name="Kiss"
        )
        self.artist_alb.album.append(self.album)

        self.genre = Genres(
            genre_id=1,
            genre="Metall"
        )
        self.musiccomposition = MusicComposition(
            mc_id=1,
            name="Hotel California",
            artist=[],
            album=[],
            year=1976,
            length=time(0, 0, 0),
            hesh="",
            priority=1,
            genre=[]
        )
        self.album_mc.composition.append(self.musiccomposition)
        self.artist_single.single.append(self.musiccomposition)
        self.musiccomposition_gen = MusicComposition(
            mc_id=2,
            name="Hotel California",
            artist=[],
            album=[],
            year=1976,
            length=time(0, 0, 0),
            hesh="mia",
            priority=1,
            genre=[]
        )
        self.musiccomposition_gen.genre.append(self.genre)

    def GetAlbum(self, lab_id):
        if lab_id == 2:
            return self.album_mc
        else:
            return self.album

    def GetArtist(self, art_id):
        if art_id == 1:
            return self.artist
        if art_id == 2:
            return self.artist_alb
        if art_id == 3:
            return self.artist_single

    def GetMusicComposition(self, mc_id):
        if mc_id == 2:
            return self.musiccomposition_gen
        else:
            return self.musiccomposition

    def GetGenre(self, gen_id):
        # self.genre_id = gen_id
        return self.genre

    def GenerateNewMCId(self):
        return 100500

    def GenerateNewArtistId(self):
        return 42

    def GenerateNewAlbumID(self):
        return 666

    def GenerateNewGenreID(self):
        return 11

    def SaveNewSingleToArtist(self, artist, mc):
        self.saved_mc = mc
        self.saved_artist = artist

    def SaveNewCompositionInAlbum(self, album, mc):
        self.saved_mc = mc
        self.saved_album = album

    def SaveNewArtist(self, artist):
        self.saved_artist = artist

    def SaveNewAlbum(self, album):
        self.saved_album = album

    def SaveNewGenre(self, genre):
        self.saved_genre = genre

    def SaveArtist(self, artist):
        self.saved_artist = artist

    def SaveAlbum(self, album):
        self.saved_album = album

    def SaveGenre(self, genre):
        self.saved_genre = genre

    def SaveMusicComposition(self, composition):
        self.saved_composition = composition


class TestUseCases(unittest.TestCase):

    def test_AddNewMCinAlbum(self):
        mc_arg = {
            "name": "com name",
            "year": 1997,
            "priority": 5
        }
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)

        actual_id = uc.AddNewMCinAlbum(50, mc_arg)

        self.assertEqual(actual_id, 100500)
        self.assertEqual(db_spy.saved_album, db_spy.album)
        self.assertEqual(db_spy.saved_mc.name, "com name")
        self.assertEqual(db_spy.saved_mc.year, 1997)
        self.assertEqual(db_spy.saved_mc.priority, 5)
        self.assertEqual(db_spy.saved_album.composition[0], db_spy.saved_mc)

    def test_AddNewSingleToArtist(self):
        mc_arg = {
            "name": "com name",
            "year": 1997,
            "priority": 5
        }
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        actual_id = uc.AddNewSingleToArtist(1, mc_arg)

        self.assertEqual(actual_id, 100500)
        self.assertEqual(db_spy.saved_artist, db_spy.artist)
        self.assertEqual(db_spy.saved_mc.name, "com name")
        self.assertEqual(db_spy.saved_mc.year, 1997)
        self.assertEqual(db_spy.saved_mc.priority, 5)
        self.assertEqual(db_spy.saved_artist.single[0], db_spy.saved_mc)

    def test_addNewArtist(self):
        art_arg = {
            "artist_name": "Skrillex"
        }
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        actual_id = uc.addNewArtist(art_arg)

        self.assertEqual(actual_id, 42)
        self.assertEqual(db_spy.saved_artist.artist_name, "Skrillex")

    def test_addNewAlbumToArtist(self):
        alb_arg = {
            "title": "Comeblack"
        }
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)

        actual_id = uc.addNewAlbumToArtist(1, alb_arg)

        self.assertEqual(actual_id, 666)
        self.assertTrue(db_spy.saved_artist.album[0])
        self.assertEqual(db_spy.saved_artist.album[0].title, alb_arg["title"])

    def test_addNewGenreToMC(self):
        gen_arg = {
            "genre": "Rock"
        }
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)

        actual_id = uc.addNewGenreToMC(99, gen_arg)

        self.assertEqual(db_spy.saved_composition.genre[0], db_spy.saved_genre)
        self.assertTrue(db_spy.saved_composition.genre[0])
        self.assertEqual(db_spy.saved_composition.genre[0].genre, gen_arg["genre"])

    def test_addCurrentGenreToMC(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        uc.addCurrentGenreToMC(1, 1)
        self.assertTrue(db_spy.saved_composition.genre[0])
        self.assertEqual(db_spy.saved_composition.genre[0].genre_id, 1)
        self.assertEqual(db_spy.saved_composition.genre[0].genre, "Metall")

    def test_ChangeCompositionPriority(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        uc.ChangeCompositionPriority(1, 10)

        self.assertEqual(db_spy.saved_composition.priority, 10)

    def test_ChangeCompositionLength(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        uc.ChangeCompositionLength(1, time(0, 3, 42))

        self.assertEqual(db_spy.saved_composition.length, time(0, 3, 42))

    def test_ChangeArtistName(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        uc.ChangeArtistName(1, "Nightwish")

        self.assertEqual(db_spy.saved_artist.artist_id, 1)
        self.assertEqual(db_spy.saved_artist.artist_name, "Nightwish")

    def test_ChangeAlbumTitle(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        uc.ChangeAlbumTitle(1, "Load")

        self.assertEqual(db_spy.saved_album.album_id, 1)
        self.assertEqual(db_spy.saved_album.title, "Load")

    def test_ChangeGenreName(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        uc.ChangeGenreName(1, "J-Rock")

        self.assertEqual(db_spy.saved_genre.genre_id, 1)
        self.assertEqual(db_spy.saved_genre.genre, "J-Rock")

    def test_AddFileToComposition(self):
        db_spy = AddNewMCDbSpy()
        blackbox_spy = BlackBoxTest()
        uc = componenta.UseCase(db_spy, blackbox_spy)
        uc.AddFileToComposition(1, "Test file.mp3")

        self.assertTrue(db_spy.saved_composition.hesh)

    def test_DeleteFileFromComposition(self):
        db_spy = AddNewMCDbSpy()
        blackbox_spy = BlackBoxTest()
        uc = componenta.UseCase(db_spy, blackbox_spy)
        uc.DeleteFileFromComposition(2)

        self.assertFalse(db_spy.saved_composition.hesh)

    def test_DeleteGenreFromComposition(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        gen_arg = db_spy.genre
        uc.DeleteGenreFromComposition(2, gen_arg)

        self.assertFalse(db_spy.saved_composition.genre)

    def test_DeleteSingleFromArtist(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        single_arg = db_spy.musiccomposition
        uc.DeleteSingleFromArtist(3, single_arg)

        self.assertFalse(db_spy.saved_artist.single)

    def test_DeleteAlbumFromArtist(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        alb_arg = db_spy.album
        uc.DeleteAlbumFromArtist(2, alb_arg)

        self.assertFalse(db_spy.saved_artist.album)

    def test_DeleteCompositionFromAlbum(self):
        db_spy = AddNewMCDbSpy()
        uc = componenta.UseCase(db_spy, None)
        mc_arg = db_spy.musiccomposition
        uc.DeleteCompositionFromAlbum(2, mc_arg)

        self.assertFalse(db_spy.saved_album.composition)


if __name__ == '__main__':
    unittest.main()
