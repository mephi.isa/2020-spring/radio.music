from datetime import time
import psycopg2
import datetime
from classes import MusicComposition, Artist, Album, Genres
from mutagen.mp3 import MP3


class Storage:
    def Validate(self, login):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("SELECT * from e01_users where login='" + login + "'")
        return cursor

    def CommitNewAlbum(self, title):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO E04_Album (title) VALUES "
        cursor.execute(query + "('" + title + "')" + " " + "ON CONFLICT DO NOTHING")
        connection.commit()
        cursor.close()

    def CommitDeleteAlbum(self, title):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "DELETE FROM E04_album WHERE title='" + title + "'"
        cursor.execute(query)
        connection.commit()

    def CommitDeleteMC(self, mc):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        current_id = connection.cursor()
        current_id.execute("Select mc_id from e02_music_composition where name='" + mc + "'")
        record_mc = current_id.fetchall()
        cursor.close()
        cursor = connection.cursor()
        query = "DELETE FROM e02_music_composition WHERE mc_id=" + str(record_mc[0][0])
        cursor.execute(query)
        connection.commit()
        cursor.close()
        connection.close()

    def CommitDeleteSingle(self, single):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        current_id = connection.cursor()
        current_id.execute("Select mc_id from e02_music_composition where name='" + single + "'")
        record_mc = current_id.fetchall()
        cursor.close()
        cursor = connection.cursor()
        query = "DELETE FROM e02_music_composition WHERE mc_id=" + str(record_mc[0][0])
        cursor.execute(query)
        connection.commit()
        cursor.close()
        connection.close()

    def CommitDeleteArtist(self, artist):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "DELETE FROM E03_artist WHERE name='" + artist + "'"
        cursor.execute(query)
        connection.commit()
        cursor.close()
        connection.close()

    def CommitCreateNewGenre(self, gen):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO E06_Genres (Genre) VALUES "
        cursor.execute(query + "('" + gen + "')" + " " + "ON CONFLICT DO NOTHING")
        connection.commit()
        cursor.close()
        connection.close()

    def CommitDeleteGenreFromDb(self, genre):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "DELETE FROM E06_Genres WHERE genre='" + genre + "'"
        cursor.execute(query)
        connection.commit()
        cursor.close()
        connection.close()

    def CommitAddAlbumToArtist(self, album, artist):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO E34_artist_album (artist_id, album_id) VALUES "
        cursor.execute(query + "(" + str(artist[0][0]) + "," + str(
            album[0][0]) + ")" + " " + "ON CONFLICT DO NOTHING")
        connection.commit()
        cursor.close()

    def CommitAddSingleToArtist(self, artist, single):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        current_id = connection.cursor()
        current_id.execute("Select artist_id from e03_artist where name='" + artist + "'")
        record_artist = current_id.fetchall()
        cursor.close()
        cursor = connection.cursor()
        query = "Update e02_music_composition set artist_id="
        cursor.execute(
            query + str(record_artist[0][0]) + " where e02_music_composition.name=" + "'" + str(single) + "'")
        connection.commit()
        cursor.close()

    def CommitAddMCinAlbum(self, mc, album):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        current_id = connection.cursor()
        current_id.execute("Select mc_id from e02_music_composition where name='" + mc + "'")
        record_mc = current_id.fetchall()
        cursor.close()
        cursor = connection.cursor()
        current_id = connection.cursor()
        current_id.execute("Select album_id from e04_album where title='" + album + "'")
        record_album = current_id.fetchall()
        cursor.close()
        cursor = connection.cursor()
        query = "INSERT INTO E42_album_composition (mc_id, album_id) VALUES "
        cursor.execute(query + "(" + str(record_mc[0][0]) + "," + str(
            record_album[0][0]) + ")" + " " + "ON CONFLICT DO NOTHING")
        connection.commit()
        cursor.close()

    def GetArtistsList(self):  # для списка исполнителей
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("select * from e03_artist")
        return cursor

    def GetArtistAlbums(self, chosen_artist):  # для списка альбомов исполнителя
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute(
            "select e04_album.album_id, e04_album.title from  e03_artist join e34_artist_album on e03_artist.artist_id=e34_artist_album.artist_id join e04_album on e34_artist_album.album_id=e04_album.album_id where e03_artist.name ='" + chosen_artist + "'")
        return cursor

    def GetMCList(self, chosen_album):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute(
            "select e2.mc_id, e2.name, e2.year, e2.length, e2.hash, e2.priority from e02_music_composition e2 left outer join e03_artist e3 on e2.artist_id=e3.artist_id left outer join e42_album_composition e42 on e2.mc_id=e42.mc_id left outer join e04_album e4 on e42.album_id=e4.album_id where e4.title='" + chosen_album + "'")
        return cursor

    def GetSinglesList(self, chosen_artist):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute(
            "select e2.mc_id, e2.name, e2.year, e2.length, e2.hash, e2.priority from e02_music_composition e2 join e03_artist e3 on e2.artist_id=e3.artist_id where e3.name='" + chosen_artist + "'")
        return cursor

    def GetAllSingles(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute(
            "select e2.mc_id, e2.name from e02_music_composition e2 left join e03_artist on e2.artist_id = e03_artist.artist_id left join e42_album_composition e42 on e2.mc_id=e42.mc_id where e42.album_id is null and e03_artist.name is null order by e2.mc_id asc")
        return cursor

    def GetAllMC(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("select * from e02_music_composition where artist_id is NULL")
        return cursor

    def GetAllAllSingles(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("select * from e02_music_composition where artist_id is not NULL")
        return cursor

    def GetAlbumsList(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute(
            "select * from  e04_album")
        return cursor

    def GetGenresList(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("select * from e06_genres")
        return cursor

    def GetCompositionGenre(self, com_id):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute(
            "select e6.genre from e02_music_composition e2 join e26_mc_genres e26 on e2.mc_id=e26.mc_id join e06_genres e6 on e26.genre_id=e6.genre_id where e2.mc_id='" + str(
                com_id[0]) + "'")
        return cursor

    def GetAlbum(self, alb_id):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("Select * from e04_album where title='" + str(alb_id) + "'")
        record = cursor.fetchall()
        new_alb = Album(record[0][0], record[0][1])
        return new_alb

    def GetArtist(self, art_id):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("Select * from e03_artist where name='" + str(art_id) + "'")
        record = cursor.fetchall()
        new_art = Artist(record[0][0], record[0][1])
        return new_art

    def GetMusicComposition(self, mc_id):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute(
            "select e2.mc_id, e2.artist_id, e2.name, e2.year, e2.length, e2.hash, e2.priority from e02_music_composition e2 where e2.mc_id=" + str(
                mc_id))
        MC = cursor.fetchall()
        new_mc = MusicComposition(MC[0][0], "None", MC[0][1], MC[0][2], MC[0][3], MC[0][4], MC[0][5], MC[0][6], "None")
        return new_mc

    def GetGenre(self, gen_id):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute(
            "select e6.genre_id, e6.genre from e06_genres e6 where e6.genre='" + str(gen_id) + "'")
        gen = cursor.fetchall()
        new_gen = Genres(gen[0][0], gen[0][1])
        return new_gen

    def GenerateNewMCId(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("select max(mc_id) from e02_music_composition")
        id_cur = cursor.fetchall()
        id = id_cur[0][0]
        id = id + 1
        cursor.close()
        return id

    def GenerateNewArtistId(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("select max(artist_id) from e03_artist")
        id_cur = cursor.fetchall()
        id = id_cur[0][0]
        id = id + 1
        cursor.close()
        return id

    def GenerateNewAlbumID(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("select max(album_id) from e04_album")
        id_cur = cursor.fetchall()
        id = id_cur[0][0]
        id = id + 1
        cursor.close()
        return id

    def GenerateNewGenreID(self):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        cursor.execute("select max(genre_id) from e06_genres")
        id_cur = cursor.fetchall()
        id = id_cur[0][0]
        id = id + 1
        cursor.close()
        return id

    def SaveNewSingleToArtist(self, artist, mc):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO E02_music_composition (artist_id, name, year, priority) VALUES "
        query_data = (query + "(" + str(artist.artist_id) + ", " + "'" + str(mc.name) + "'" + ", " + str(
            mc.year) + ", " + str(mc.priority) + ")" + " " + "ON CONFLICT DO NOTHING")
        cursor.execute(query_data)
        connection.commit()
        cursor.close()

    def SaveNewCompositionInAlbum(self, album, mc):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO E02_music_composition (name, year, priority) VALUES "
        query_data = (
                query + "(" + "'" + str(mc.name) + "'" + ", " + str(mc.year) + ", " + str(
            mc.priority) + ")" + " " + "ON CONFLICT DO NOTHING")
        cursor.execute(query_data)
        connection.commit()
        cursor.close()

        cursor = connection.cursor()
        query = "INSERT INTO E42_album_composition (mc_id, album_id) VALUES "
        query_data = (query + "(" + str(mc.mc_id) + ", " + str(album.album_id) + ")" + " " + "ON CONFLICT DO NOTHING")
        cursor.execute(query_data)
        connection.commit()
        cursor.close()

    def SaveNewArtist(self, artist):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO E03_Artist (name) VALUES "
        cursor.execute(query + "('" + str(artist.artist_name) + "')" + " " + "ON CONFLICT DO NOTHING")
        connection.commit()
        cursor.close()

    def SaveNewAlbum(self, album):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO E04_Album (title) VALUES "
        cursor.execute(query + "('" + str(album.title) + "')" + " " + "RETURNING album_id")
        connection.commit()
        cursor.close()

    def SaveNewGenre(self, genre):
        pass

    def SaveGenreAfterRename(self, genre):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "UPDATE E06_Genres SET genre='" + str(genre.genre) + "'" + " " + "where genre_id = " + str(
            genre.genre_id)
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveAlbumAfterChangeTitle(self, alb):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "UPDATE E04_Album SET title='" + str(alb.title) + "'" + " " + "where album_id = " + str(alb.album_id)
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveAlbumAfterDeleteMC(self, mc, alb):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "Delete from E42_album_composition where mc_id='" + str(
            mc.mc_id) + "'" + " " + "and album_id = '" + str(alb.album_id) + "'"
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveArtistAfterChangeName(self, art):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "UPDATE E03_Artist SET name='" + str(art.artist_name) + "'" + " " + "where artist_id = " + str(
            art.artist_id)
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveArtistAfterDeleteSingle(self, mc):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "UPDATE e02_music_composition SET artist_id=" + "NULL" + " " + "where mc_id = " + str(mc.mc_id) + ""
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveArtistAfterAddNewAlbum(self, artist, album):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO E34_artist_album (artist_id, album_id) VALUES "
        cursor.execute(
            query + "(" + str(artist.artist_id) + "," + str(album.album_id) + ")" + " " + "ON CONFLICT DO NOTHING")
        connection.commit()
        cursor.close()

    def SaveArtistAfterDeleteAlbum(self, artist, album):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "Delete from E34_artist_album where artist_id='" + str(
            artist.artist_id) + "'" + " " + "and album_id = '" + str(album.album_id) + "'"
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveMusicCompositionAfterFile(self, mc):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "UPDATE e02_music_composition SET length='" + str(mc.length) + "'" + ", hash ='" + str(
            mc.hesh) + "'" + "where mc_id = '" + str(mc.mc_id) + "'"
        cursor.execute(query)
        connection.commit()

    def SaveMusicCompositionAfterChangePriority(self, mc):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "UPDATE e02_music_composition SET priority ='" + str(mc.priority) + "'" + " " + "where mc_id = " + str(
            mc.mc_id)
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveMusicCompositionAfterAddGenre(self, mc_id, gen_id):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "INSERT INTO e26_mc_genres (mc_id,genre_id) VALUES (" + str(mc_id.mc_id) + "," + str(
            gen_id.genre_id) + ")"
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveMusicCompositionAfterDeleteGenre(self, mc_id, gen_id):
        connection = psycopg2.connect("dbname=music user=postgres password=12345")
        cursor = connection.cursor()
        query = "Delete from E26_mc_genres where mc_id='" + str(mc_id.mc_id) + "'" + " " + "and genre_id = '" + str(
            gen_id.genre_id) + "'"
        cursor.execute(query)
        connection.commit()
        cursor.close()

    def SaveMusicComposition(self, composition):
        pass


class BB:
    def SaveFileToBlackBox(self, file_name):
        current_hash = hash(file_name)
        return current_hash

    def DeleteFileFromBlackBox(self, file):
        pass


db = Storage()
black_box = BB()


def CheckCompositionFields(new_mc):
    if new_mc.name == "":
        err_msg = "Name is NULL!"
        raise ValueError(err_msg)
    if new_mc.year == "":
        err_msg = "Year is NULL!"
        raise ValueError(err_msg)
    if new_mc.length == "":
        err_msg = "Length is NULL!"
        raise ValueError(err_msg)
    if new_mc.priority == "":
        err_msg = "Priority is NULL!"
        raise ValueError(err_msg)


def CheckArtistFields(new_art):
    if new_art.artist_name == "":
        err_msg = "Name is NULL!"
        raise ValueError(err_msg)


def CheckAlbumFields(new_alb):
    if new_alb.title == "":
        err_msg = "Title is NULL!"
        raise ValueError(err_msg)


def CheckGenreFields(new_gen):
    if new_gen.genre == "":
        err_msg = "Genre is NULL!"
        raise ValueError(err_msg)


class UseCase:
    def __init__(self, db, black_box):
        self.__db = db
        self.__blackbox = black_box

    def AddNewMCinAlbum(self, album_id, s_name, s_year, s_priority):  # usecase
        album = self.__db.GetAlbum(album_id)
        new_mc = MusicComposition(
            mc_id=self.__db.GenerateNewMCId(),
            artist=[],
            album=[],
            hesh="",
            length=time(0),
            genre=[],
            name=s_name,
            year=s_year,
            priority=s_priority
        )
        CheckCompositionFields(new_mc)
        album.add_composition(new_mc)
        self.__db.SaveNewCompositionInAlbum(album, new_mc)
        return new_mc.mc_id

    def AddNewSingleToArtist(self, artist_id, s_name, s_year, s_priority):
        artist = self.__db.GetArtist(artist_id)
        new_single = MusicComposition(
            mc_id=self.__db.GenerateNewMCId(),
            artist=[],
            album=[],
            hesh="",
            length=time(0),
            genre=[],
            name=s_name,
            year=s_year,
            priority=s_priority
        )
        CheckCompositionFields(new_single)
        artist.add_single(new_single)
        self.__db.SaveNewSingleToArtist(artist, new_single)
        return new_single.mc_id

    def addNewArtist(self, item):
        new_art = Artist(
            artist_id=self.__db.GenerateNewArtistId(),
            artist_name=item
        )
        CheckArtistFields(new_art)
        self.__db.SaveNewArtist(new_art)
        return new_art.artist_id

    def addNewAlbumToArtist(self, artist_id, item):
        artist = self.__db.GetArtist(artist_id)
        new_alb = Album(
            album_id=self.__db.GenerateNewAlbumID(),
            title=item
        )
        CheckAlbumFields(new_alb)
        artist.add_album(new_alb)
        self.__db.SaveNewAlbum(new_alb)
        self.__db.SaveArtistAfterAddNewAlbum(artist, new_alb)
        return new_alb.album_id

    def addNewGenreToMC(self, mc_id, item):  # не реализовано как избыточное
        mc = self.__db.GetMusicComposition(mc_id)
        new_gen = Genres(
            genre_id=self.__db.GenerateNewGenreID(),
            **item
        )
        CheckGenreFields(new_gen)
        mc.add_genre(new_gen)
        self.__db.SaveNewGenre(new_gen)
        self.__db.SaveMusicComposition(mc)
        return new_gen.genre_id

    def addCurrentGenreToMC(self, mc_id, gen_id):
        mc = self.__db.GetMusicComposition(mc_id)
        gen = self.__db.GetGenre(gen_id)
        mc.add_genre(gen)
        self.__db.SaveMusicCompositionAfterAddGenre(mc, gen)

    # sets
    def ChangeCompositionPriority(self, com_id, priority):
        mc = self.__db.GetMusicComposition(com_id)
        mc.set_priority(priority)
        self.__db.SaveMusicCompositionAfterChangePriority(mc)
        return mc.priority

    def ChangeCompositionLength(self, com_id, length):  # не реализовано как избыточное
        mc = self.__db.GetMusicComposition(com_id)
        mc.set_length(length)
        self.__db.SaveMusicComposition(mc)
        return mc.length

    def ChangeArtistName(self, art_id, name):
        art = self.__db.GetArtist(art_id)
        art.set_artist_name(name)
        self.__db.SaveArtistAfterChangeName(art)
        return art.artist_name

    def ChangeAlbumTitle(self, alb_id, title):
        alb = self.__db.GetAlbum(alb_id)
        alb.set_title(title)
        self.__db.SaveAlbumAfterChangeTitle(alb)
        return alb.title

    def ChangeGenreName(self, gen_id, genre):
        gen = self.__db.GetGenre(gen_id)
        gen.set_genre(genre)
        self.__db.SaveGenreAfterRename(gen)
        return gen.genre

    # sets

    def AddFileToComposition(self, com_id, file):  # usecase
        hesh = self.__blackbox.SaveFileToBlackBox(file)
        mc = self.__db.GetMusicComposition(com_id)
        mc.add_file(hesh)
        length = self.ExtractLengthFromFile(file)
        mc.set_length(length)
        self.__db.SaveMusicCompositionAfterFile(mc)

    def ExtractLengthFromFile(self, file_name):  # по идее этого здесь быть не должно но я не смог удержаться
        audio = MP3(file_name)
        length = datetime.timedelta(seconds=audio.info.length)
        return length

    def DeleteFileFromComposition(self, mc_id):
        mc = self.__db.GetMusicComposition(mc_id)
        self.__blackbox.DeleteFileFromBlackBox(mc.hesh)
        mc.delete_file()
        self.__db.SaveMusicCompositionAfterFile(mc)

    def DeleteGenreFromComposition(self, mc_id, gen_id):
        mc = self.__db.GetMusicComposition(mc_id)
        gen_item = self.__db.GetGenre(gen_id)  # костыль
        mc.genre.append(gen_item)  # костыль
        mc.delete_genre(gen_item)
        self.__db.SaveMusicCompositionAfterDeleteGenre(mc, gen_item)

    # Artist
    def DeleteSingleFromArtist(self, art_id, single_id):
        art = self.__db.GetArtist(art_id)
        single_item = self.__db.GetMusicComposition(single_id)  # костыль
        art.single.append(single_item)
        art.delete_single(single_item)
        self.__db.SaveArtistAfterDeleteSingle(single_item)

    def DeleteAlbumFromArtist(self, art_id, alb_id):
        art = self.__db.GetArtist(art_id)
        alb_item = self.__db.GetAlbum(alb_id)  # костыль
        art.album.append(alb_item)  # костыль
        art.delete_album(alb_item)
        self.__db.SaveArtistAfterDeleteAlbum(art, alb_item)

    # Artist

    def DeleteCompositionFromAlbum(self, alb_id, com_item):
        alb = self.__db.GetAlbum(alb_id)
        com = self.__db.GetMusicComposition(com_item)  # костыль
        alb.composition.append(com)  # костыль
        alb.delete_composition(com)
        self.__db.SaveAlbumAfterDeleteMC(com)

    def DeleteArtist(self, art_id):
        pass
