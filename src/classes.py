from datetime import time


class Genres:
    def __init__(
        self,
        genre_id: int,
        genre: str
    ):
        self.genre_id: int = genre_id
        self.genre: str = genre

    def get_genre_id(self):
        return self.genre_id

    def get_genre(self):
        return self.genre

    def set_genre(self, genre: str):
        self.genre = genre


class MusicComposition:
    def __init__(
        self,
        mc_id: int,
        name: str,
        artist: list,
        album: list,
        year: int,
        length: time,
        hesh: str,
        priority: float,
        genre: list
    ):
        self.mc_id: int = mc_id
        self.name: str = name
        self.artist: list = []
        self.album: list = []
        self.year: int = year
        self.length: time = length
        self.hesh: str = hesh
        self.priority: float = priority
        self.genre: list = []

    def get_mc_id(self):
        return self.mc_id

    def get_name(self):
        return self.name

    def get_year(self):
        return self.year

    def get_length(self):
        return self.length

    def get_hesh(self):
        return self.hesh

    def get_priority(self):
        return self.priority

    def set_priority(self, priority):
        self.priority = priority

    def set_length(self, length):#кастом для изменения продолжительности вручную при загрузке файла
        self.length = length

    def add_genre(self, genre_obj):
        self.genre.append(genre_obj)

    def delete_genre(self, genre_obj):
        self.genre.remove(genre_obj)

    def get_all_genres(self):
        return self.genre

    def get_artist(self):
        return self.artist

    def get_album(self):
        return self.album

    def add_artist(self, artist_obj):
        if not self.get_artist():
            self.artist.append(artist_obj)

    def delete_artist(self):
        self.artist.remove(self.artist[0])

    def add_album(self, album_obj):
        if not self.get_album():
            self.album.append(album_obj)

    def delete_album(self):
        self.album.remove(self.album[0])

    def add_file(self, hesh):
        self.hesh = hesh

    def delete_file(self):
        self.hesh = None


class Album:
    def __init__(
        self,
        album_id: int,
        title: str
    ):
        self.album_id: int = album_id
        self.title: str = title
        self.composition: list = []

    def get_album_id(self):
        return self.album_id

    def get_title(self):
        return self.title

    def set_title(self, title: str):
        self.title = title

    def add_composition(self, composition_obj):
        self.composition.append(composition_obj)

    def delete_composition(self, composition_obj):
        self.composition.remove(composition_obj)

    def get_all_composition(self):
        return self.composition


class Artist:
    def __init__(
        self,
        artist_id: int,
        artist_name: str
    ):
        self.artist_id: int = artist_id
        self.artist_name: str = artist_name
        self.album: list = []
        self.single: list = []

    def get_artist_id(self):
        return self.artist_id

    def get_artist_name(self):
        return self.artist_name

    def set_artist_name(self, artist_name: str):
        self.artist_name = artist_name

    def add_album(self, album_obj):
        self.album.append(album_obj)

    def add_single(self, single_obj):
        self.single.append(single_obj)

    def delete_album(self, album_obj):
        self.album.remove(album_obj)

    def delete_single(self, single_obj):
        self.single.remove(single_obj)

    def get_all_albums(self):
        return self.album

    def get_all_singles(self):
        return self.single


class ThematicPlaylist:
    def __init__(self, tp_id: int, playlist_name: str, description: str):
        self.tp_id: int = tp_id
        self.playlist_name: str = playlist_name
        self.composition: list = []
        self.description: str = description

    def get_tp_id(self):
        return self.tp_id

    def get_playlist_name(self):
        return self.playlist_name

    def get_all_compositions(self):
        return self.composition

    def add_composition(self, composition_obj):
        self.composition.append(composition_obj)

    def delete_composition(self, composition_obj):
        self.composition.remove(composition_obj)